from django.shortcuts import render
from datetime import datetime

# Create your views here.
def home(request):
	now = datetime.now()
	return render(request, 'base.html', {'today': now})